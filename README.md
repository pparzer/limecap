# LimeCap

An external REDCap module for managing LimeSurvey surveys from REDCap. The module is most useful in combination with the [LimeSurvey Study Page](https://gitlab.com/pparzer/limesurvey_study_page).

## Table of contents

- [Motivation](#motivation)
- [Installation](#installation)
- [Usage](#usage)
  - [Create surveys in LimeSurvey](#create-surveys-in-limesurvey)
  - [Create REDCap forms](#create-redcap-forms)
  - [Configure the module](#configure-the-module)
  
## Motivation

REDCap offers surveys for your study participants. However, there are use cases where you'd prefer to use LimeSurvey for your surveys. LimeSurvey has more options to create sophisticated and complex surveys, and the multilingual support of LimeSurvey is way ahead of the language support of REDCap surveys.

Another application is the combination of study instruments with instruments for everyday clinical practice. If you use LimeSurvey for questionnaires for everyday clinical practice, then it makes sense to also use LimeSurvey for additional study instruments so that patients can use the same interface for both kind of questionnaires.


## Installation

- Create the directory `limecap_<VERSION>` in your REDCap module directory, where `<VERSION>` is the content of the file `VERSION`. For example, if the version is `v1.2`, the module directory on Linux would be `/var/www/html/redcap/modules/limecap_v1.2`. Make sure that this directory is writable by the web server. The web server creates `.log` files in this directory for debugging and error logging.

- Copy all files from this directory into the module directory. Make sure that all files can be read by the web server.

- Activate the module in the *External Module Manager* of the *Control Center* in REDCap.

- Configure the module in the *External Module Manager*. You have to provide at least the LimeSurvey API URL of your LimeSurvey server. The API URL can be found under *Interfaces* in the *Global Settings* of your LimeSurvey server.

**Make sure, the JSON-RPC interface in LimeSurvey is enabled.**

## Usage

### Create surveys in LimeSurvey

The surveys must include two questions of type *Equation* to connect the participant responses in LimeSurvey to the correct record, event, and instance in REDCap:

- A hidden question with the code `record` and the equation `{TOKEN:LASTNAME}`,

- and a hidden question with the code `event` and the equation `{TOKEN:FIRSTNAME}`.

These two questions should be the first questions of the survey to ensure that they are filled in with the values of the fields *First name* and *Last name* from the participant information before the other questions are processed. For the languages `EN` and `DE` you can import the group `LimeCap.lsg` from the `tools` directory to include these two questions in your survey.

When a participant is added to a LimeSurvey survey by the REDCap module LimeCap, the field `Last Name` of the participant information is filled with the record ID of REDCap and the field `First Name` with the event ID of REDCap. If the REDCap project uses instances, the instance ID is appended to the event ID, separated by a period.

### Create REDCap forms
 
 For each survey you need a corresponding status form in REDCap in order to track the status of the participant responses in LimeSurvey. This form must contain at least the following fields (replace <form\> with the internal form name in REDCap):

 | Field Name         | Type     | Label       | Choices |
 |--------------------|----------|-------------|---------|
 |<form\>_validfrom  | datetime | Valid from  |         |
 |<form\>_validuntil | datetime | Valid until |         |
 |<form\>_status     | dropdown | Status      | 1=new, 2=activated, 3=completed, 4=expired |
 |<form\>_startdate  | datetime | Started     |         |
 |<form\>_submitdate | datetime | Completed   |         |

The fields `Status`, `Started`, and `Completed` should be readonly. **The default value for the status field must be `'1'`.**

`Valid from` and `Valid to` define the period in which the participant can take the questionnaire. The `Status` field shows the status of the data record in LimeSurvey. 
- `new`: The record in REDCap exists, but the corresponding participant in LimeSurvey has not yet been created.
- `activated`: The corresponding participant information is added to the LimeSurvey survey, the questionnaire is active for the participant, and the participant has not yet completed the questionnaire.
- `completed`: The participant has completed the questionnaire in LimeSurvey.
- `expired`: The questionnaire was not completed during the validity period, the questionnaire is no longer available in LimeSurvey for this participant.

When the participant has completed the questionnaire, the fields `Started` and `Completed` contain the timestamp of when the survey was started and ended.

For the languages `EN` and `DE` you can use the R script `make-redcap-limesurvey-form.R` in the `tools` directory to create zip files to import the status forms in REDCap.

**Important: If you change the form name, you must also change the field names. Otherwise the module will fail to update the field values.**

### Configure the module

#### Site wide configuration

*LimeSurvey API URL*
: The API URL of your LimeSurvey server. You find the API URL under *Interfaces* in the *Global Settings* of your LimeSurvey server. Note that all projects that use the LimeCap module connect to the same LimeSurvey server.

*Proxy host*
: The host name of your proxy if there is a firewall between the REDCap server and the LimeSurvey server.

*Proxy authentification*
: If your proxy requires authentication, enter your credentials in the form `Username:Password`.

#### Project specific configuration

*LimeSurvey username*
: The username to connect to the LimeSurvey server. You should create a unique user for each study.

*LimeSurvey password*
: The password for the LimeSurvey user.

*LimeSurvey code field*
: The name of the REDCap field, that is used to store the code for the participant. This code must be unique for each participant and serves as keyword for participation in the LimeSurvey questionnaire. The LimeCap module ensures that a unique code is generated if this field is empty for a participant. There should only exist one instance of the code field for each participant.

*Study code prefix* (optional)
: If you use the same participant code in different REDCap projects, you can specify a study specific prefix.The LimeSurvey participant keyword is created by adding this prefix to the code to ensure that the LimeSurvey keyword is unique to this project.

Configure for each survey the connections of the REDCap status forms to the corresponding LimeSurvey surveys.

*REDCap instrument name*
: The name of the REDCap status form.

*LimeSurvey survey ID*
: the survey ID of the LimeSurvey survey that corresponds to the REDCap status form.

*Instrument specific code appendix* (optional)
: The content of this field is appended to the keyword for this instrument only. This is useful when you have multiple respondents for a study participant. For example, if the study participant is a child, the questionnaires for the child have the code as the LimeSurvey keyword. In order to provide parents with different keywords, you can for example append `'f'` to the code for the child's father's questionnaires and `'m'` to the code for the child's mother's questionnaires. In this way you can ensure that each respondent has their own keyword.

If the code field of a study participant is empty, the LimeCap module generates a unique random number for this participant as code.

*Number of code digits* (optional)
: Specify the number of digits for the generated code. The default is 5 digits.

If you need additional participant information for your LimeSurvey surveys, you can specify the REDCap fields that are passed as additional attributes to the LimeSurvey participant information. The content of these fields can be used in LimeSurvey as `TOKEN:ATTRIBUTE_1`, `TOKEN:ATTRIBUTE_2`, ...

*Attribute field*
: The name of the REDCap field whose content will be passed as attribute to the LimeSurvey participant information.

